<?php

/**
 * Base components for the sfFoxCookbookPlugin sfFoxCookbookFrontend module.
 *
 * @package     sfFoxCookbookPlugin
 * @subpackage  sfFoxCookbookFrontend
 * @author      Mauro D'Alatri <mauro.dalatri@bee-lab.net>
 */
abstract class BasesfFoxCookbookFrontendComponents extends sfComponents
{

  /**
   * checkConfiguration
   *
   */
  public function checkConfiguration()
  {

    $connection   = $this->dbConnection;
    $routePrefix  = $this->routePrefix;
    if (!isset($connection, $routePrefix))
    {
      throw new DomainException('Missing info in module.yml');
    }
    $aConnection = Propel::getConnection($connection);

    return Array(
      'connection'  => $connection,
      'routePrefix' => $routePrefix,
      'aConnection' => $aConnection
    );

  }

  /**
   * Executes lastRecipeBox
   *
   * @params sfWebRequest $request
   *
   */

  public function executeLastRecipeBox(sfWebRequest $request)
  {
    $conf = $this->checkConfiguration();

    $this->con = $conf['aConnection']; 

    $this->recipe = FoxCookbookRecipeQuery::create()->
      orderById('desc')->
      setLimit(1)->
      findOne($conf['aConnection']);


  }

  /**
   * Executes latestRecipesBox
   *
   * @params sfWebRequest $request
   *
   */

  public function executeLatestRecipesBox(sfWebRequest $request)
  {
    $conf = $this->checkConfiguration();
    
    $this->con = $conf['aConnection']; 

    $lastRecipe = FoxCookbookRecipeQuery::create()->
      orderById('desc')->
      setLimit(1)->
      findOne($conf['aConnection']);

    $idLastRecipe = (isset($lastRecipe)) ? $lastRecipe->getId() : false;

    $this->recipes = FoxCookbookRecipeQuery::create()->
      orderById('desc')->
      _if($idLastRecipe)->
        where('id <> ' . $idLastRecipe)->
      _endif()->
      find($conf['aConnection']);

  }



}
