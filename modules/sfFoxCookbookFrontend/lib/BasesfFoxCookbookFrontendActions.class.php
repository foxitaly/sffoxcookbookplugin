<?php

/**
 * Base actions for the sfFoxCookbookPlugin sfFoxCookbookFrontend module.
 * 
 * @package     sfFoxCookbookPlugin
 * @subpackage  sfFoxCookbookFrontend
 * @author      Mauro D'Alatri <m.dalatri@consultingalpha.net>
 * @version     SVN: $Id: BaseActions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
abstract class BasesfFoxCookbookFrontendActions extends sfActions
{

  /**
   * Set needed variables
   *
   * @param sfRequest $request A request object
   */
  public function preExecute()
  {
    $module         = $this->getRequest()->getParameter('module');
    $this->connectionName = sfConfig::get('mod_' . strtolower($module) . '_db_connection_name');
    $this->routePrefix = sfConfig::get('mod_' . strtolower($module) . '_route_prefix');
    $this->howManyRecipes = sfConfig::get('mod_' . strtolower($module) . '_items_pager',20);
    if (!isset($this->connectionName, $this->routePrefix))
    {
      throw new DomainException('Missing info in module.yml');
    }
    $this->dbConnection = Propel::getConnection($this->connectionName);
  }


  /**
   * Execute recipe
   *
   */
  public function executeRecipe(sfWebRequest $request)
  {
    $this->recipe =  FoxCookBookRecipeQuery::create()->findPk($request->getParameter('idRecipe',null),$this->dbConnection);
    $this->forward404If(!$this->recipe);
  }

  /**
   * Execute recipes
   *
   */
  public function executeRecipes(sfWebRequest $request)
  {
    $this->recipes        = FoxCookBookRecipeQuery::create()->orderById('DESC')->paginate($request->getParameter('page',1), $this->howManyRecipes, $this->dbConnection);
    $this->currentRecipe  = current($this->recipes->getResults());

    $this->forward404If(!$this->currentRecipe);

  }

}
