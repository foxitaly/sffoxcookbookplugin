<ul>
  <?php foreach($recipes as $recipe): ?>
  <li>
  	<div class="foto"><img src="<?php echo $recipe->getThumb('70x70'); ?>"></div>
    <div class="descrizione">
        <p class="type"><i><?php echo $recipe->getCategory($con->getRawValue()); ?></i></p>
        <h5 class="title"><a class="rlink" href="<?php echo url_for('@cucinedaincubo_recipe?slugShow=' . $show->getI18nSlug() . '&idRecipe=' . $recipe->getId()); ?>"><?php echo $recipe->getTitle(); ?></a></h5>
        <!--small class="time">Tempo: <strong><?php #echo $recipe->getTimeForPrepare(); ?></strong></small><br /-->
        <div class="label">Preparazione:&nbsp;</div>
        <div class="<?php echo $recipe->getLevel(); ?>"></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </li>
  <?php endforeach; ?>
</ul>

