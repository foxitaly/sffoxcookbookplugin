<a href="<?php echo url_for('@cucinedaincubo_recipe?slugShow=' . $show->getI18nSlug() . '&idRecipe=' . $recipe->getId()); ?>" title="<?php echo $recipe->getTitle(); ?>" class="playVideo">PLAY</a>
<a href="<?php echo url_for('@cucinedaincubo_recipe?slugShow=' . $show->getI18nSlug() . '&idRecipe=' . $recipe->getId()); ?>" title="<?php echo $recipe->getTitle(); ?>" class="thumbVideo">
    <img src="<?php echo $recipe->getVideoThumb("280x160"); ?>" title="<?php echo $recipe->getTitle(); ?>" alt="<?php echo $recipe->getTitle(); ?>" />
</a>
<p><?php echo $recipe->getCategory($con->getRawValue())?></p>
<h5><a href="<?php echo url_for('@cucinedaincubo_recipe?slugShow=' . $show->getI18nSlug() . '&idRecipe=' . $recipe->getId()); ?>"><?php echo $recipe->getTitle(); ?></a></h5>
<div class="info">
	<div class="label">Preparazione:&nbsp;</div>
    <div class="<?php echo $recipe->getModeForPrepare();?>"><?php echo $recipe->getModeForPrepare();?></div>
    <div class="clear"></div>
</div>
