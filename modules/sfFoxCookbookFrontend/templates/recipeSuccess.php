<?php $sf_response->setTitle($show . " | " . $sf_response->getTitle()); ?>

<?php $aDescription = ($show->getMetaDescription()) ? $show->getMetaDescription() : $show->getSynopsis(); ?>
<?php $aKeywords    = ($show->getMetaKeywords())    ? $show->getMetaKeywords()    : null; ?>
<?php $aImage    	= (!isset($aImage))        ? image_path($show->getCover(),Array('absolute' => true))  : $aImage; ?>
<?php $aType        = 'tv_show'; ?>

<?php slot('metaGeneric'); ?>
<?php include_partial('global/metaGeneric', Array(
  'description' => $aDescription,
  'keywords'    => $aKeywords
)); ?>
<?php end_slot(); ?>


<?php slot('ogGeneric'); ?>
  <?php include_partial('global/metaOgGeneric', Array(
    'ogDescription' => $aDescription,
    'ogImage'       => $aImage,
    'ogType'        => $aType
  )); ?>
<?php end_slot(); ?>

<?php slot('linkRel'); ?>
  <?php include_partial('global/linkRel', Array(
    'image_src' => $aImage
  )); ?>
<?php end_slot(); ?>

<?php use_javascript('gigyaHelper.js'); ?>
<script type="text/javascript">getNComments(<?php echo $show->getId();?>);</script>

<?php  include_partial('show/showCover',Array('show'=>$show))?>

<div class="module620 minisito">

  <div class="recipe">

    Dessert

    <div>
      <iframe src="http://fox.crosscast-system.com/embed/?v=d28d2380-4425-4bfb-9c66-86feff748ffb&s=fox&noembed=1&norelated=1&noquality=1" type="text/html" width="620" height="350" frameborder="0" allowtransparency="true" class="vPlayer"></iframe>
    </div>

    <p>
      <strong>Tempo:</strong> 5 Minuti - <strong>Difficolt&agrave;:</strong> Media
    </p>

    <p class="title">Titolo della ricetta</p>

    <div class="list">
      <img src="http://placehold.it/620x300&text=Ingredienti">
    </div>

    <div>
      <div><img src="http://placehold.it/200x200" class="photoRecipe"></div>
      <div><img src="http://placehold.it/200x200" class="photoRecipe"></div>
      <div><img src="http://placehold.it/200x200" class="photoRecipe"></div>
    </div>


    <div class="index">
      <a href="#">Torna alla lista</a>
    </div>

  </div>

</div>

<div class="module300 paddingRight10 last">
    <?php include_partial('show/showColDx',Array('show'=>$show,'noTvGuide'=>true, 'noShowExtraBox'=>false, 'noLastVideo'=>false, 'noLastNews'=>false)); ?>
</div>

<div class="clear"></div>

