<?php

require_once dirname(__FILE__).'/../lib/BasesfFoxCookbookBackendActions.class.php';

/**
 * sfFoxCookbookBackend actions.
 * 
 * @package    sfFoxCookbookPlugin
 * @subpackage sfFoxCookbookBackend
 * @author     Mauro D'Alatri <m.dalatri@consultingalpha.net>
 * @version    SVN: $Id: actions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
class sfFoxCookbookBackendActions extends BasesfFoxCookbookBackendActions
{
}
