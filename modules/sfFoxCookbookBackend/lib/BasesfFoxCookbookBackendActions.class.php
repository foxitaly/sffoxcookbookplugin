<?php

/**
 * Base actions for the sfFoxCookbookPlugin sfFoxCookbookBackend module.
 * 
 * @package     sfFoxCookbookPlugin
 * @subpackage  sfFoxCookbookBackend
 * @author      Mauro D'Alatri <m.dalatri@consultingalpha.net>
 * @version     SVN: $Id: BaseActions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
abstract class BasesfFoxCookbookBackendActions extends sfActions
{
}
