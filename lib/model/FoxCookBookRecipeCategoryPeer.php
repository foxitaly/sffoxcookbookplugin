<?php



/**
 * Skeleton subclass for performing query and update operations on the 'fox_cook_book_recipe_category' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.7.0-dev on:
 *
 * Fri Apr 19 11:20:52 2013
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.plugins.sfFoxCookbookPlugin.lib.model
 */
class FoxCookBookRecipeCategoryPeer extends BaseFoxCookBookRecipeCategoryPeer
{
}
