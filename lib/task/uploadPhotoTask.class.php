<?php

class uploadPhotoTask extends sfBaseTask
{
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'cucinedaincubo'),
      // add your own options here
    ));

    $this->namespace        = 'sfFoxCookBook';
    $this->name             = 'uploadPhoto';
    $this->briefDescription = '';
    $this->detailedDescription = <<<EOF
The [uploadPhoto|INFO] task does things.
Call it with:

  [php symfony uploadPhoto|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
    // initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
    $connection = $databaseManager->getDatabase("cucinedaincubo")->getConnection();

    // add your code here

    $photos = FoxCookBookRecipeQuery::create()->find($connection);

    foreach($photos as $photo)
    {
          
      $FPath = sfConfig::get('sf_data_dir') . DIRECTORY_SEPARATOR . "sfFoxCookBookPluginPhoto" . DIRECTORY_SEPARATOR . basename($photo->getDefaultImage());

      $clearPath = $photo->getDefaultImage();

      $this->logSection('PHOTO', "http://www.foxtv.it/uploads/photo/" . $clearPath);
      $this->logSection('PHOTO END', $FPath);
      $this->logSection('', '');


      $ch = curl_init("http://www.foxtv.it/uploads/photo/" . $clearPath);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      //curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
      $rawdata=curl_exec($ch);
    

      curl_close ($ch);
      if(file_exists($FPath)){
        unlink($FPath);
      }
        $fp = fopen($FPath,"w+");
        fwrite($fp, $rawdata);
        fclose($fp);
    }

    //$con = Propel::getConnection(ChannelPeer::DATABASE_NAME);
    //echo $con->getLastExecutedQuery();
  }


}





