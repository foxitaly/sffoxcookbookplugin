<?php

/**
 * FoxCookBookRecipe filter form.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage filter
 * @author     ##AUTHOR_NAME##
 */
class FoxCookBookRecipeFormFilter extends BaseFoxCookBookRecipeFormFilter
{
  public function configure()
  {
    $dbConnection = $this->getOption('dbConnection',false);

    $this->widgetSchema['category_id'] =  new sfWidgetFormPropelChoice(array(
      'connection'  => $dbConnection,
      'model'       => 'FoxCookBookRecipeCategory',
      'add_empty'   => false
    ));

  }
}
