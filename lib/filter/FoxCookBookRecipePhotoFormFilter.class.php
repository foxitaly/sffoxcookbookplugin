<?php

/**
 * FoxCookBookRecipePhoto filter form.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage filter
 * @author     ##AUTHOR_NAME##
 */
class FoxCookBookRecipePhotoFormFilter extends BaseFoxCookBookRecipePhotoFormFilter
{
  public function configure()
  {
    $dbConnection = $this->getOption('dbConnection',false);

    $this->widgetSchema['recipe_id'] = new sfWidgetFormPropelChoice(array('connection' => $dbConnection, 'model' => 'FoxCookBookRecipe', 'add_empty' => false));

  }
}
