<?php

/**
 * FoxCookBookRecipeCategory form.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage form
 * @author     ##AUTHOR_NAME##
 */
class FoxCookBookRecipeCategoryForm extends BaseFoxCookBookRecipeCategoryForm
{

  public function configure()
  {

    $dbConnection = $this->getOption('dbConnection',false);

    //if(!$dbConnection) Throw new Exception('You must define which DB connection do you want');

    $this->validatorSchema->setPostValidator(
      new sfValidatorPropelUnique(array('connection' => $dbConnection, 'model' => 'FoxCookBookRecipeCategory', 'column' => array('title')))
    );
  }
}
