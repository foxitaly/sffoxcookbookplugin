<?php

/**
 * FoxCookBookRecipe form.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage form
 * @author     ##AUTHOR_NAME##
 */
class FoxCookBookRecipeForm extends BaseFoxCookBookRecipeForm
{
  public function configure()
  {

    unset($this->widgetSchema['level']);

    $this->levelChoices = array(
      'Facile' => "Facile",
      'Media' => "Media",
      'Difficile' => "Difficile",
    );

    $this->priceChoices = array(
      'Basso' => "Basso",
      'Medio' => "Medio",
      'Alto' => "Alto",
    );
    

    $dbConnection = $this->getOption('dbConnection',false);

    $this->widgetSchema['category_id'] =  new sfWidgetFormPropelChoice(array(
      'connection'  => $dbConnection, 
      'model'       => 'FoxCookBookRecipeCategory', 
      'add_empty'   => false
    ));


    $this->widgetSchema['default_image'] = new sfWidgetFormInputFileEditable(array(
      'file_src'     => '/uploads/photo/' . $this->getObject()->getDefaultImage(),
      'is_image'     => true,
      'with_delete'  => false,
    ));

   $this->widgetSchema['ingredients_image'] = new sfWidgetFormInputFileEditable(array(
      'file_src'     => '/uploads/photo/' . $this->getObject()->getIngredientsImage(),
      'is_image'     => true,
      'with_delete'  => false,
    ));


    $this->widgetSchema['price'] = $w = new sfWidgetFormChoice(array(
      'choices' => $this->priceChoices
    )); 
    $this->widgetSchema['mise_en_place'] = $w = new sfWidgetFormChoice(array(
      'choices' => $this->levelChoices
    )); 
    $this->widgetSchema['mode_for_prepare'] = $w = new sfWidgetFormChoice(array(
      'choices' => $this->levelChoices
    )); 



    $this->validatorSchema['default_image'] = new sfImageFileValidator(array(
     'required'   => is_null($this->getObject()->getDefaultImage()),
     'path'       => sfConfig::get('sf_upload_dir') . '/photo',
     'mime_types' => 'web_images'
    ));
    $this->validatorSchema['ingredients_image'] = new sfImageFileValidator(array(
     'required'   => is_null($this->getObject()->getIngredientsImage()),
     'path'       => sfConfig::get('sf_upload_dir') . '/photo',
     'mime_types' => 'web_images'
    ));



    $this->validatorSchema['category_id'] = new sfValidatorPropelChoice(array(
      'connection'  => $dbConnection,
      'model'       => 'FoxCookBookRecipeCategory', 
      'column'      => 'id'
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorPropelUnique(array('connection' => $dbConnection, 'model' => 'FoxCookBookRecipe', 'column' => array('title')))
    );

  }
}
