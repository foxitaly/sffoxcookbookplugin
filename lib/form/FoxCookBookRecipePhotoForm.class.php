<?php

/**
 * FoxCookBookRecipePhoto form.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage form
 * @author     ##AUTHOR_NAME##
 */
class FoxCookBookRecipePhotoForm extends BaseFoxCookBookRecipePhotoForm
{
  public function configure()
  {
    
    $dbConnection = $this->getOption('dbConnection',false);

    $this->widgetSchema['recipe_id'] = new sfWidgetFormPropelChoice(array('connection' => $dbConnection, 'model' => 'FoxCookBookRecipe', 'add_empty' => false));


    $this->widgetSchema['file'] = new sfWidgetFormInputFileEditable(array(
      'file_src'     => '/uploads/photo/' . $this->getObject()->getFile(),
      'is_image'     => true,
      'with_delete'  => false,
    ));


    $this->validatorSchema['file'] = new sfImageFileValidator(array(
      'required'   => is_null($this->getObject()->getFile()),
      'path'       => sfConfig::get('sf_upload_dir') . '/photo',
      'mime_types' => 'web_images'
    ));

    $this->validatorSchema['recipe_id'] = new sfValidatorPropelChoice(array('connection' => $dbConnection, 'model' => 'FoxCookBookRecipe', 'column' => 'id'));

    $this->validatorSchema->setPostValidator(
      new sfValidatorPropelUnique(array(
        'connection' => $dbConnection, 
        'model' => 'FoxCookBookRecipePhoto', 
        'column' => array('title')
      ))
    );

  }
}
